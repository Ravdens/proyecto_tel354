#from sre_parse import State
from ..state import State
from ..startup import opciones_combustible,opciones_tipo_vehiculo,opciones_transmision
from pagina.templates import template

import reflex as rx


@template(route="/estimador_ev", title="Estimar Vehiculo Eléctrico")
def estimador_ev() -> rx.Component:
    return rx.vstack(
        rx.heading("Estimar rendimiento de vehículo a combustible", size="8"),
        rx.text("Marca del auto"),
        rx.input(
            max_length=20,
            value=State.Make,
            on_change=State.set_Make
        ),
        rx.text("Modelo"),
        rx.input(
            max_length=20,
            value=State.Model,
            on_change=State.set_Model
        ),
        rx.text("Año"),
        rx.input(
            max_length=20,
            value=State.Year,
            on_change=State.set_Year,
            type="number",
        ),
        rx.text("Número de cilindros del motor"),
        rx.input(
            max_length=20,
            value=State.Cylinders,
            on_change=State.set_Cylinders,
            type="number",
        ),
        rx.text("Tamaño del motor (en Litros)"),
        rx.input(
            max_length=20,
            value=State.Engine_size,
            on_change=State.set_Engine_size,
            type="number",
        ),
        rx.text("Tipo de vehiculo"),
        rx.select(
            opciones_tipo_vehiculo,
            placeholder="Seleccionar",
            value=State.Vehicle_class,
            on_change=State.set_Vehicle_class
        ),
        rx.text("Tranmision"),
        rx.select(
            opciones_transmision,
            placeholder="Seleccionar",
            value=State.Transmission,
            on_change=State.set_Transmission
        ),
        rx.text("Tipo de combustible"),
        rx.select(
            opciones_combustible,
            placeholder="Seleccionar",
            value=State.Fuel_type,
            on_change=State.set_Fuel_type
        ),
        rx.button(
            "Estimar",
            color_scheme="grass",
            on_click=State.handle_estimar_gas
        ),
        rx.cond(
            State.show_estimacion,
            rx.vstack(
                rx.divider(size="4"),
                rx.text(State.City_L100km),
                rx.text(State.Highway_L100km),
                rx.data_table(
                    data=State.get_similares_dataframe,
#                    pagination=True,
#                    search=True,
                    sort=True,
                    resizable=True,
                    style={"font-size": "5px", "max-height": "100px", "overflow-y": "visible"},
                ),
            ),

        ),

    )
