# Codigo de inicio de la pagina

import pandas as pd
import numpy as np
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import OneHotEncoder
import time

print("Inicializando...")

def to_dummies(data,encoder,variables):
    d_num=data.drop(columns=variables)
    d_enc=encoder.transform(data[variables])
    return d_num.reset_index(drop=True).join(pd.DataFrame(d_enc,columns=encoder.get_feature_names_out()))
    
def entrenar_algoritmo(nombre_algoritmo,dataframe,vars):
    x_vars = vars["x"]
    y_vars = vars["y"]
    ign_vars = vars["ign"]
    cat_vars = vars["cat"]
    start_time = time.time()
    enc = OneHotEncoder(handle_unknown='ignore',sparse_output=False)
    enc.fit(dataframe[cat_vars])
    df_d = to_dummies(dataframe,enc,cat_vars)
    rem = y_vars + ign_vars
#    X_train, X_test, Y_train, Y_test = train_test_split(df_d.drop(columns=rem), df_d[y_vars], test_size = 0.3,random_state=1)
    X=df_d.drop(columns=rem)
    Y=df_d[y_vars]
    krr = KernelRidge(alpha=1,kernel="rbf")
    krr.fit(X,Y)
    print("Entrenado algoritmo "+nombre_algoritmo+" en "+str(time.time() - start_time)+" segundos.")
    return [krr,enc]

def predict_consumption(x,krr,enc,vars):
    d = to_dummies(x,enc,vars["cat"])
    y_p = krr.predict(d)
    return pd.DataFrame(y_p,columns=vars["y"])

def n_mas_cercanos(dato,columnas,dataset,n):
    df1= pd.concat([dato]*len(dataset.index))
    df2= dataset[columnas]
    df2["distancia"]=np.linalg.norm(df1[columnas].values - df2[columnas].values, axis=1)
    return dataset.iloc[df2.sort_values(by='distancia').head(n).index]

pd.options.mode.chained_assignment = None  # default='warn'


#Vehiculos de combustion
dfs=[]
archivos_gas = ["my1995-2004-fuel-consumption-ratings-5-cycle",
                "my2005-2014-fuel-consumption-ratings-5-cycle",
               "my2015-2019-fuel-consumption-ratings",
               "my2020-fuel-consumption-ratings",
               "my2021-fuel-consumption-ratings",
               "my2022-fuel-consumption-ratings",
               "my2023-fuel-consumption-ratings",
               "my2024-fuel-consumption-ratings"]
for f in archivos_gas:
    dfs.append(pd.read_csv("archive/"+f+".csv",encoding="ISO-8859-1"))
df_gas = pd.concat(dfs).reset_index(drop=True)

gas_vars = {}
gas_vars["x"] = ["Model year","Engine size (L)","Cylinders"]
gas_vars["y"] =["City (L/100 km)","Highway (L/100 km)"]
gas_vars["ign"] = ["CO2 rating","Smog rating","CO2 emissions (g/km)","Combined (mpg)","Combined (L/100 km)"]
gas_vars["cat"] = ["Make","Model","Vehicle class","Transmission","Fuel type"]

df_gas_train, df_gas_test = train_test_split(df_gas, test_size = 0.3,random_state=1)

#Vehiculos electricos
df_ev = pd.read_csv("archive/my2012-2024-battery-electric-vehicles.csv",encoding="ISO-8859-1")

ev_vars = {}
ev_vars["x"] = ["Model year","Motor (kW)"]
ev_vars["y"] = ["City (kWh/100 km)","Highway (kWh/100 km)",'Range (km)','Recharge time (h)']
ev_vars["ign"] = ["CO2 rating ","Smog rating","CO2 emissions (g/km)","Combined (kWh/100 km)",'City (Le/100 km)',
       'Highway (Le/100 km)', 'Combined (Le/100 km)',"Fuel type"]
ev_vars["cat"] = ["Make","Model","Vehicle class","Transmission"]

df_ev_train, df_ev_test = train_test_split(df_ev, test_size = 0.3,random_state=1)

#Vehiculos híbridos
df_hb = pd.read_csv("archive/my2012-2024-plug-in-hybrid-electric-vehicles.csv",encoding="ISO-8859-1")

hb_vars = {}
hb_vars["x"]= ["Model year","Motor (kW)","Engine size (L)","Cylinders"]
hb_vars["y"] = ["City (L/100 km)","Highway (L/100 km)",'Range 1 (km)','Range 2 (km)','Recharge time (h)']
hb_vars["ign"] = ["Combined Le/100 km","Combined (L/100 km)","CO2 emissions (g/km)","CO2 rating","Smog rating"]
hb_vars["cat"] = ["Make","Model","Vehicle class","Transmission","Fuel type 1","Fuel type 2"]

df_hb_train, df_hb_test = train_test_split(df_hb, test_size = 0.3,random_state=1)


[algoritmo_combustible,encoder_combustible]=entrenar_algoritmo("Combustible",df_gas_train,gas_vars)
[algoritmo_electricos,encoder_electricos]=entrenar_algoritmo("Electricos", df_ev_train,ev_vars)
[algoritmo_hibridos,encoder_hibridos]=entrenar_algoritmo("Híbrido", df_hb_train,hb_vars)

opciones_transmision = list(pd.concat([df_gas["Transmission"],df_ev["Transmission"],df_hb["Transmission"]]).unique())
opciones_tipo_vehiculo = list(pd.concat([df_gas["Vehicle class"],df_ev["Vehicle class"],df_hb["Vehicle class"]]).unique())
opciones_combustible = list(pd.concat([df_gas["Fuel type"],df_hb["Fuel type 1"],df_hb["Fuel type 2"]]).unique())

Y_pred = predict_consumption(df_gas_test[gas_vars["x"]+gas_vars["cat"]], algoritmo_combustible, encoder_combustible,gas_vars)
errores_gas = {}
for c in gas_vars["y"]:
    errores_gas[c]=(mean_squared_error(df_gas_test[c],Y_pred[c]))**0.5

Y_pred = predict_consumption(df_ev_test[ev_vars["x"]+ev_vars["cat"]],algoritmo_electricos, encoder_electricos,ev_vars)
errores_ev = {}
for c in ev_vars["y"]:
    errores_ev[c]=(mean_squared_error(df_ev_test[c],Y_pred[c]))**0.5

Y_pred = predict_consumption(df_hb_test[hb_vars["x"]+hb_vars["cat"]],algoritmo_hibridos,encoder_hibridos,hb_vars)
errores_hb = {}
for c in hb_vars["y"]:
    errores_hb[c]=(mean_squared_error(df_hb_test[c],Y_pred[c]))**0.5

