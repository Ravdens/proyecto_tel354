import reflex as rx
import pandas as pd

#from .startup import predict_consumption
from .startup import *

class State(rx.State):

    #Combustible    
    #Inputs
    Make:str =""
    Model:str=""
    Year:int=0
    Cylinders:int=0
    Engine_size:int=0
    Vehicle_class:str=""
    Transmission:str=""
    Fuel_type:str=""
    #Outputs
    City_L100km:str="Rendimiento en ciudad: ----- (L/100km)"
    Highway_L100km:str="Rendimiento en carretera: ----- (L/100km)"
    #Similares
    Gas_Similares:dict={}

    #Page vars
    show_estimacion:bool = False

    def handle_estimar_gas(self):
        dataframe = pd.DataFrame({
            "Model year":self.Year,
            "Make":self.Make,
            "Model":self.Model,
            "Vehicle class":self.Vehicle_class,
            "Engine size (L)":self.Engine_size,
            "Cylinders":self.Cylinders,
            "Transmission":self.Transmission,
            "Fuel type":self.Fuel_type
        },index=[0])
        res=predict_consumption(dataframe,algoritmo_combustible,encoder_combustible,gas_vars)
        self.City_L100km="Rendimiento en ciudad: "+str(round(res.values.tolist()[0][0],4))+" \u00B1 "+str(round(errores_gas["City (L/100 km)"],4))+" (L/100km)"
        self.Highway_L100km="Rendimiento en carretera: "+str(round(res.values.tolist()[0][1],4))+" \u00B1 "+str(round(errores_gas["Highway (L/100 km)"],4)) +" (L/100km)"
        mas_cercanos = n_mas_cercanos(res,gas_vars["y"],df_gas,10)
        self.Gas_Similares= mas_cercanos[gas_vars["cat"]+gas_vars["x"]+gas_vars["y"]].to_dict() 
        self.estimacion = res.to_dict()
        self.show_estimacion=True


    @rx.var
    def get_similares_dataframe(self) -> pd.DataFrame:
        return pd.DataFrame.from_dict(self.Gas_Similares)