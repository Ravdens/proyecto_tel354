# Bienvenido a el estimador de rendimiento de vehiculos

## ¿Qué es esto?

La siguiente página es una interfaz para utilizar el algoritmo entrenado para el proyecto final de el grupo cu4tro de minería de datos (TEL354). Utilizando este, se puede estimar el rendimiento de uso energético de un vehiculo a base de sus características físicas. El algoritmo que realiza estas estimaciónes está entrenado usando los datos de la [siguiente página](https://fcr-ccc.nrcan-rncan.gc.ca/en?_gl=1*1d104ub*_ga*MTU3MzQ3OTE2NS4xNzEyMDk1NDU4*_ga_C2N57Y7DX5*MTcxOTcxMDMyMC40LjEuMTcxOTcxMDMyOS4wLjAuMA).


## Cómo usar

## Vehículos convencionales/híbridos

### Caracteristicas

Año del modelo: el año utilizado por el fabricante para designar un modelo de vehículo. Para ayudarlo a comparar vehículos de diferentes años de modelo, las clasificaciones de consumo de combustible para vehículos de 1995 a 2014 se han ajustado para reflejar el procedimiento de prueba de 5 ciclos. Tenga en cuenta que estos son valores aproximados que se generaron a partir de las calificaciones originales, no de las pruebas del vehículo.

Marca: El fabricante del vehículo.	

Modelo: El nombre del modelo del vehículo AWD = Tracción en todas las ruedas: vehículo diseñado para funcionar con todas las ruedas propulsadas 4WD/4X4 = Tracción en las cuatro ruedas: vehículo diseñado para funcionar con dos o cuatro ruedas propulsadas FFV = Vehículo de combustible flexible – vehículo diseñado para funcionar con mezclas de gasolina y etanol de hasta un 85% de etanol (E85) GNC = Gas natural comprimido; GNV = Vehículo a gas natural SWB = Batalla corta; LWB = Distancia entre ejes larga; EWB = Distancia entre ejes extendida # = Motor de alto rendimiento

Clase de vehículo: clasificación del vehículo basada en el volumen interior para automóviles y el peso bruto vehicular para camionetas ligeras. Para obtener más información, visite https://natural-resources.canada.ca/energy-efficiency/transportation-alternative-fuels/personal-vehicles/choosing-right-vehicle/buying-electric-vehicle/understanding-the-tables/21383

Tamaño del motor (L): Cantidad total de todos los cilindros (en litros) 

Cilindros: Número de cilindros del motor.	 

Transmisión: El tipo de transmisión y el número de marchas/velocidades A = Automática; AM = Manual automatizado; AS = Automático con turno seleccionado; AV = Continuamente variable; M=Manual

Tipo de combustible: El tipo de combustible utilizado para impulsar el vehículo X = Gasolina regular; Z = Gasolina premium; D = Diésel; E = E85; N = Gas Natural Para los FFV, los valores de consumo se proporcionan tanto para gasolina como para E85.


### Resultados

Ciudad (L/100 km) Clasificación de consumo de combustible en ciudad mostrada en litros cada 100 kilómetros. La clasificación de ciudad representa la conducción urbana en tráfico intermitente.

Carretera (L/100 km): La clasificación de consumo de combustible en carretera se muestra en litros cada 100 kilómetros. La clasificación en carretera representa una combinación de conducción en autopista abierta y en caminos rurales, típica de viajes más largos.


## Vehículos eléctricos híbridos enchufables		

### Características

Año del modelo: el año utilizado por el fabricante para designar un modelo de vehículo. Para ayudarlo a comparar vehículos de diferentes años de modelo, las clasificaciones de consumo de combustible para vehículos de 1995 a 2014 se han ajustado para reflejar el procedimiento de prueba de 5 ciclos. Tenga en cuenta que estos son valores aproximados que se generaron a partir de las calificaciones originales, no de las pruebas del vehículo.

Marca: El fabricante del vehículo.	 

Modelo: El nombre del modelo del vehículo AWD = Tracción total: vehículo diseñado para funcionar con tracción en todas las ruedas.

Clase de vehículo: clasificación del vehículo basada en el volumen interior para automóviles y el peso bruto vehicular para camionetas ligeras. Para obtener más información, visite https://natural-resources.canada.ca/energy-efficiency/transportation-alternative-fuels/personal-vehicles/choosing-right-vehicle/buying-electric-vehicle/understanding-the-tables/21383entender-las-tablas/21383 

Motor (kW): Potencia máxima de salida del motor eléctrico (en kilovatios)

Tamaño del motor (L): Cilindrada total de todos los cilindros (en litros) 

Cilindros: Número de cilindros del motor. 

Transmisión: El tipo de transmisión y el número de marchas/velocidades A = Automática; AM = Manual automatizado; AS = Automático con turno seleccionado; AV = Continuamente variable

Tipo de combustible 1: El tipo de combustible utilizado para impulsar el vehículo en modo eléctrico B = Electricidad; X = gasolina regular; Z = Gasolina premium En modo eléctrico, un vehículo eléctrico híbrido enchufable en serie utiliza únicamente electricidad. Un vehículo eléctrico híbrido enchufable mixto puede funcionar utilizando electricidad únicamente o tanto electricidad como gasolina. Un asterisco (*) indica que en las pruebas, este vehículo no utilizó gasolina durante el funcionamiento en modo eléctrico. Sin embargo, dependiendo de cómo conduzca el vehículo, podrá utilizar gasolina durante el funcionamiento en modo eléctrico después de una carga completa.

Tipo de combustible 2: El tipo de combustible utilizado para impulsar el vehículo en modo solo gasolina X = Gasolina regular; Z = gasolina premium

### Resultados

Ciudad (L/100 km): La clasificación de consumo de combustible en ciudad para el modo solo de gasolina se muestra en litros cada 100 kilómetros. La clasificación en ciudad representa la conducción urbana en tráfico intermitente.

Carretera (L/100 km): La clasificación de consumo de combustible en carretera para el modo de solo gasolina se muestra en litros cada 100 kilómetros. La clasificación en carretera representa una combinación de conducción en autopista abierta y en caminos rurales, típica de viajes más largos.

Rango 1 (km): distancia de conducción estimada (en kilómetros) con la batería completamente cargada 

Rango 2 (km): Distancia de conducción estimada (en kilómetros) con el depósito lleno de combustible 

Tiempo de recarga (h): Tiempo estimado (en horas) para recargar completamente la batería a 240 voltios 


## Vehículos eléctricos de batería

### Características

Año del modelo: el año utilizado por el fabricante para designar un modelo de vehículo. Para ayudarlo a comparar vehículos de diferentes años de modelo, las clasificaciones de consumo de combustible para vehículos de 1995 a 2014 se han ajustado para reflejar el procedimiento de prueba de 5 ciclos. Tenga en cuenta que estos son valores aproximados que se generaron a partir de las calificaciones originales, no de las pruebas del vehículo.

Marca: El fabricante del vehículo.	 

Modelo: El nombre del modelo del vehículo AWD = Tracción total: vehículo diseñado para funcionar con tracción en todas las ruedas. 
Clase de vehículo: clasificación del vehículo basada en el volumen interior para automóviles y el peso bruto vehicular para camionetas ligeras. Para obtener más información, visite https://natural-resources.canada.ca/energy-efficiency/transportation-alternative-fuels/personal-vehicles/choosing-right-vehicle/buying-electric-vehicle/understanding-the-tables/21383

Motor (kW): potencia máxima de salida del motor eléctrico (en kilovatios)	 

Transmisión: El tipo de transmisión y el número de marchas/velocidades A = Automática

Tipo de combustible: El tipo de combustible utilizado para impulsar el vehículo B = Electricidad


### Resultados

Ciudad (kWh/100 km): clasificación de consumo de combustible en ciudad mostrada en kilovatios hora cada 100 kilómetros. La clasificación de ciudad representa la conducción urbana en tráfico con paradas y arranques.

Autopista (kWh/100 km): clasificación de consumo de combustible en carretera mostrada en kilovatios hora cada 100 kilómetros. La clasificación de carretera representa una combinación de conducción en autopistas abiertas y caminos rurales, típica de viajes más largos.

Rango (km): la distancia de conducción estimada (en kilómetros) con una batería completamente cargada	

Tiempo de recarga (h): Tiempo estimado (en horas) para recargar completamente la batería a 240 voltios